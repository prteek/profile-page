
![skills](./26BDD1DD-9BD8-4C96-B43F-7FA8445C9A76.jpeg)

### Mathematical modelling and data science Engineer for Hybrid electric vehicle development

#### Contact : <prateekpatel.in@gmail.com>  

I strive for brilliance in the things I like to do. For this I focus strongly on fundamentals like statistics, physics and deductive reasoning for approaching problems and adhere to simple but thorough solutions because I believe that often the brilliant solutions and ideas are the simplest ones.

### Past Experience
#### Altran, London — Consultant Engineer
JUNE 2016 - PRESENT <br/>
Consulting Engineer at Ford Dunton Technical Centre in Electrified Powertrain Engineering team
* Mathematical modelling of hybrid electric system features for feasibility analysis of Ford’s first 48V system.
* Statistical analysis of system usage in real world to predict system requirements.
  Product: Transit/Puma 48V mHEV systems
* Optimised Statistical model tool chain for faster 30x fast turnaround of analysis durability assessment 
  
#### Affluent Technologies, Bangalore — Senior Engineer
OCTOBER 2015 - APRIL 2016  <br/>
CAE Analyst at Jaguar Land Rover (Gaydon) in  Performance, Efficiency and Drivability team
* Simulation Model correlations for performance and FE tests of D7a (Jaguar) Platform  
  
#### Maruti Suzuki, Gurgaon — Assistant Manager
JULY 2013 - OCTOBER 2015  <br/>
* Fleet testing and data analysis, homologation for design of Maruti’s first mild Hybrid system
  Product: Ciaz SHVS 12V hybrid
* Systems benchmarking of Toyota Camry Hybrid for function evaluation

### EDUCATION
#### Indian School of Mines, Dhanbad — Bachelor of technology, Mechanical Engineering
AUGUST 2009 - MAY 2013

### PUBLICATIONS
#### Study on regenerative braking systems, considerations of design, safety and associated effects — ITEC India  
[Researchgate link](https://www.researchgate.net/publication/302479523_Study_on_regenerative_braking_system_considerations_of_design_safety_and_associated_effects)  
#### Integrated Torque split and gear shift optimisation — ITEC India   
[Researchgate link](https://www.researchgate.net/publication/302479434_Integrated_torque_split_and_gear_shift_optimization_using_novel_technique_in_HEV)  